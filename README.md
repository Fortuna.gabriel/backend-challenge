# Teste Técnico para Desenvolvedor backend utilizando Node.js & Express

Este teste foi projetado para avaliar suas habilidades básicas em Node.js e Express para construir uma API RESTful simples para gerenciamento de pacientes.

## Descrição do Desafio

Você deverá desenvolver uma API RESTful com as seguintes funcionalidades básicas de CRUD (Create, Read, Update, Delete) para uma entidade de "Paciente".

### Requisitos

- Cadastro de paciente: Permitir o cadastro de novos pacientes com nome, e-mail, cpf e idade.
- Listagem de pacientes: Recuperar todos os pacientes cadastrados.
- Busca de paciente por ID: Encontrar um paciente específico pelo seu ID.
- Atualização de paciente: Permitir a atualização das informações de um paciente existente.
- Exclusão de paciente: Deletar um paciente pelo seu ID.

### Requisitos Técnicos

- Use Node.js e Express.js para construir a API.
- A API deve seguir os princípios RESTful.
- Utilize JSON para representar os dados.
- Não é necessário implementar autenticação/autorização.
- Armazenamento temporário pode ser feito em memória (utilizando arrays ou objetos JavaScript).

## Instruções para Execução

1. Clone este repositório.
2. Instale as dependências usando `npm install`.
3. Implemente a lógica para atender aos requisitos descritos acima.
4. Documente o funcionamento da API no arquivo README.md.
5. Teste a API para garantir que as funcionalidades estão operando corretamente.
6. Faça um commit do seu código em uma branch separada com seu nome.
7. Abra um merge request (pull request) para a branch main.

## Recursos

- [Node.js](https://nodejs.org/)
- [Express.js](https://expressjs.com/)
- [Documentação do Express](https://expressjs.com/en/4x/api.html)

## Observações

- Caso tenha alguma dúvida sobre os requisitos ou o funcionamento da API, não hesite em entrar em contato conosco.
- Se necessário, inclua quaisquer instruções adicionais ou considerações importantes para o teste.
